import { Component } from '@angular/core';
import { NavController, Platform, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Diagnostic } from '@ionic-native/diagnostic';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public platform : Platform, private diagnostic : Diagnostic, public alertCtrl: AlertController) {

  }

  //Wifi Alert
  alertWifi(){
    let wifiAlert = this.alertCtrl.create({
      title: 'Oops',
      message: 'Check Wifi connection',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            
          }
        }
      ]
    });
    wifiAlert.present();
  }

  //Location Alert
  alertLocation(){
    let locationAlert = this.alertCtrl.create({
      title: 'Oops',
      message: 'Turn On Location Services',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            
          }
        }
      ]
    });
    locationAlert.present();
  }


  //Check for location
  checkLocation(){
    if ( this.platform.is('cordova')) {
        this.platform.ready().then((readySource) => {
            this.diagnostic.isLocationEnabled().then(
                (success) => {
                    if(success == true){
                      this.diagnostic.isWifiEnabled().then(
                        (successWifi) => {
                          if(successWifi == true){
                            this.navCtrl.push(TabsPage);
                          } else {
                            this.alertWifi();
                          }
                        }
                      )
                    } else {
                        this.alertLocation();
                    }
                }
            );
        })
    } else{
        this.navCtrl.push(TabsPage);
    }
  }
}
