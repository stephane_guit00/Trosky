import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IndexPage } from '../index/index';
import { RecentPage } from '../recent/recent';

@Component ({
    selector: 'page-tabs',
    templateUrl : 'tabs.html'
})
export class TabsPage {
    tab1root = IndexPage;
    tab2root = RecentPage;
    constructor(public navCtrl : NavController){
        
    }
}