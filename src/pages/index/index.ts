import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;
@Component({
    selector : 'page-index',
    templateUrl : 'index.html'
})
export class IndexPage {

    @ViewChild('map') mapElement : ElementRef;
    map : any;

    constructor(public navCtrl: NavController, public geolocation : Geolocation){
        
    }

    ionViewDidLoad(){
        this.loadMap();
    }

    loadMap(){

        this.geolocation.getCurrentPosition().then((position) => {
            let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            
            let mapOptions = {
                center : latLng,
                zoom : 15,
                mapTypeId : google.maps.MapTypeId.ROADMAP,
            }
            
            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            let point = this.map.getCenter();
            let marker = this.setMarker(point, '- -', '../../assets/icon/location.png');


        }, (err) => {
            console.log(err);
        });
    }

    setMarker (point, html, icon) {
        let marker = new google.maps.Marker({
            map : this.map,
            animation: google.maps.Animation.DROP,
            position: point,
            icon : icon,
        });

        let content = html;

        if (html != ""){
            let infoWindow = new google.maps.InfoWindow({
                content: content
            });

            google.maps.event.addListener(marker, 'click', () => {
                this.map.setCenter(point);
                infoWindow.open(this.map, marker);
                
            });
        }
        return marker;
    }

}